<?php
if (PHP_SAPI != 'cli') {
    exit('Rodar via CLI');
}

require __DIR__ . '/vendor/autoload.php';

// Instantiate the app
$settings = require __DIR__ . '/src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/src/dependencies.php';

$db = $container->get('db');

$schema = $db->schema();
$tabela = 'produtos';

$schema->dropIfExists($tabela);

//Cria a tabela produtos
$schema->create($tabela, function($table){
    $table->increments('id');
    $table->string('titulo', 100);
    $table->text('descricao');
    $table->decimal('preco', 11, 2);
    $table->string('fabricante', 60);
    $table->timestamps();
});

//preencher a tabela produtos
$db->table($tabela)->insert([
    'titulo' => 'Portátil HP D330',
    'descricao' => 'I7 nova geracao, 16 GB RAM, NVIDIA, 1TB',
    'preco' => '2200.10',
    'fabricante' => 'HP',
    'created_at' => '2019-01-01',
    'updated_at' => '2019-01-01'
]);

$db->table($tabela)->insert([
    'titulo' => 'TV Samsung P3300',
    'descricao' => 'Smartv 4k inteligente com internet incorporada, 6000x2000 px',
    'preco' => '835.99',
    'fabricante' => 'Samsung',
    'created_at' => '2019-01-01',
    'updated_at' => '2019-01-01'
]);