<?php

use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\Produto;
use App\Models\Utilizadore;
use \Firebase\JWT\JWT;

//rotas para gerar token
$app->post('/api/token', function($request, $response){

    $dados = $request->getParsedBody();
    $email = $dados['email'] ?? null;
    $senha = $dados['senha'] ?? null;

    $utilizador = Utilizadore::where('email', $email)->first();

    if( !is_null($utilizador) && (md5($senha) === $utilizador->senha) ){
        
        //gerar token
        $secretKey = $this->get('settings')['secretKey'];
        $chaveAcesso = JWT::encode($utilizador, $secretKey);
        return $response->withJson([
            'chave' => $chaveAcesso
        ]);
    }

    return $response->withJson([
        'status' => 'erro'
    ]);


});

?>