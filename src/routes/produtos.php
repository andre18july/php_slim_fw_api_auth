<?php

use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\Produto;


$app->group('/api/v1', function(){

    //listar todos
    $this->get('/produtos/lista', function(Request $request, Response $response){
        $this->logger->info("Slim-Api '/produtos/lista' route");
        $produtos = Produto::get();
        return $response->withJson( $produtos );
    });

    //inserir novo
    $this->post('/produtos/inserir', function(Request $request, Response $response){
        $this->logger->info("Slim-Api '/produtos/insere' route");
        $dados = $request->getParsedBody();
        $produto = Produto::create($dados);
        return $response->withJson( $produto );
    });

    //obter produto por id
    $this->get('/produtos/lista/{id}', function(Request $request, Response $response, $args){
        $produto = Produto::findOrFail( $args['id'] );
        return $response->withJson( $produto );
    });


    //Atualizar produto para id
    $this->put('/produtos/atualizar/{id}', function(Request $request, Response $response, $args){
        $dados = $request->getParsedBody();
        $produto = Produto::findOrFail( $args['id'] );
        $produto->update( $dados );
        return $response->withJson( $produto );
    });


    //remover produto por id
    $this->get('/produtos/remover/{id}', function(Request $request, Response $response, $args){
        $produto = Produto::findOrFail( $args['id'] );
        $produto->delete();
        return $response->withJson( $produto );
    });





});
